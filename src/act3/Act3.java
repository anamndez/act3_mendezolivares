package act3;

/**
 *
 * @author Ana Itzel Mendez Olivares
 */
public class Act3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Integer[] ejemplo = new Integer[9];

        ejemplo[0] = 5;
        ejemplo[1] = 7;
        ejemplo[2] = 15;
        ejemplo[3] = 20;
        ejemplo[4] = 21;
        ejemplo[6] = 35;
        ejemplo[7] = 37;
        ejemplo[8] = 50;
        ejemplo[5] = 30;
        int n = 30;
        Buscador buscador = new Buscador(ejemplo);
        int a = buscador.hacerBusquedaSecuencial(n);
        System.out.println(a);
        int b = buscador.hacerBusquedaBinaria(n);
        System.out.println(b);

    }

}
