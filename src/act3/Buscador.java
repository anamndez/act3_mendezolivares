package act3;

/**
 *
 * @author Ana Itzel Mendez Olivares
 * @param <E>
 */
public class Buscador<E extends Comparable<E>> {

    E[] elementos;

    Buscador(E[] elementos) {
        this.elementos = elementos;
    }

    public void setElementos(E[] elementos) {
        this.elementos = elementos;
    }

    /**
     * Metodo que busca un elemento en un arreglo
     *
     * @param elementoBuscado
     * @return posicion del elemento buscado, -1 si el elemento buscado no se
     * encuentra en el arreglo
     */
    public int hacerBusquedaSecuencial(E elementoBuscado) {
        //suponemos que el elemento no se encuentra en el arreglo
        int resultado = -1;
        //recorremos el arreglo
        for (int i = 0; i < elementos.length; i++) {
            //Si un elemento coincide con el elemento buscado, el resultado es la posicion
            if (elementos[i].compareTo(elementoBuscado) == 0) {
                resultado = i;
            }
        }
        return resultado;
    }

    /**
     * Metodo que busca un elemento en un arreglo ordenado de forma creciente
     *
     * @param elementoBuscado
     * @return posicion del elemento buscado, -1 si el elemento buscado no se
     * encuentra en el arreglo
     */
    public int hacerBusquedaBinaria(E elementoBuscado) {
        //Suponemos que el elemento no se encuentra en el arreglo
        int resultado = -1;

        //Determinamos los limites del arreglo para encontrar la posicion media
        int limiteIzq = 0;
        int limiteDer = elementos.length;
        int mitad = elementos.length / 2;

        //Si el elemento buscado es menor que el menor del arreglo o mayor que el 
        //mayor del arreglo, entonces no se encuentra en el arreglo
        if (elementos[0].compareTo(elementoBuscado) > 0
                || elementos[elementos.length - 1].compareTo(elementoBuscado) < 0) {
            return resultado;        
        } else {

            do {

                //Si el elemento buscado es menor que la mitad,
                //el nuevo limite derecho es la mitad
                if (elementos[mitad].compareTo(elementoBuscado) >= 0) {
                    //Si el limite derecho es igual a la mitad, 
                    //entonces el elemento no se encuentra en el arreglo
                    if (limiteDer == mitad) {
                        return resultado;
                    } else {

                        limiteDer = mitad;
                    }
                }
                //Si el elemento buscado es mayor que la mitad,
                //el nuevo limite izquierdo es la mitad
                if (elementos[mitad].compareTo(elementoBuscado) <= 0) {
                    //Si el limite izquierdo es igual a la mitad, 
                    //entonces el elemento no se encuentra en el arreglo
                    if (limiteIzq == mitad) {
                        return resultado;
                    } else {

                        limiteIzq = mitad;
                    }
                }
                //Calculamos la mitad con los nuevos limites
                mitad = (limiteDer + limiteIzq) / 2;
                //Repetimos el proceso hasta encontrar un elemento que sea igual al buscado
            } while (elementos[mitad].compareTo(elementoBuscado) != 0);
            //Regresamos la posicion del elemento que es igual al buscado
            return mitad;
        }
    }

}
